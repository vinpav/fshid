#!/usr/bin/env python3
import RPi.GPIO as GPIO # Import Raspberry Pi GPIO library
import time

release_delay = .1

# key modifiers
mod_dict = {
    'nul':0,
    'shf':2,
    'ctl':1,
    'alt':4,
    'ctlshf':3,
    'altshf':6,
    'altctl':5
}

# keys
key_dict = {
    'BEACON':89,
    'LANDING_LIGHT':90,
    'TAXI_LIGHT':91,
    'NAV_LIGHT':92,
    'STROBE_LIGHT':93,
    'FUEL_PUMP':94,
    'PITOT_HEAT':95,
    'STBY_BAT':96,
    'ALT':97,
    'BAT':98,
    'AV1_ON':54,
    'AV1_OFF':51,
    'AV2_ON':99,
    'AV2_OFF':87
}

# button mapping dict
# Format : PIN : {'PIN' : 'button_pin', 'pressed_key' : 'key_dict_entry', 'released_key' : 'key_dict_entry', 'pressed_key_mod' : 'mod_dict_entry', 'released_key_mod' : 'mod_dict_entry', 'pressed' : 'button status True or False'}
mapping_dict = {
    7 : {'PIN' : 7, 'pressed_key' : 'BEACON', 'released_key' : 'BEACON', 'pressed_key_mod' : 'nul', 'released_key_mod' : 'nul', 'pressed' : False},
    8 : {'PIN' : 8, 'pressed_key' : 'LANDING_LIGHT', 'released_key' : 'LANDING_LIGHT', 'pressed_key_mod' : 'nul', 'released_key_mod' : 'nul', 'pressed' : False},
    10 : {'PIN' : 10, 'pressed_key' : 'TAXI_LIGHT', 'released_key' : 'TAXI_LIGHT', 'pressed_key_mod' : 'nul', 'released_key_mod' : 'nul', 'pressed' : False},
    11 : {'PIN' : 11, 'pressed_key' : 'NAV_LIGHT', 'released_key' : 'NAV_LIGHT', 'pressed_key_mod' : 'nul', 'released_key_mod' : 'nul', 'pressed' : False},
    12 : {'PIN' : 12, 'pressed_key' : 'STROBE_LIGHT', 'released_key' : 'STROBE_LIGHT', 'pressed_key_mod' : 'nul', 'released_key_mod' : 'nul', 'pressed' : False},
    13 : {'PIN' : 13, 'pressed_key' : 'FUEL_PUMP', 'released_key' : 'FUEL_PUMP', 'pressed_key_mod' : 'nul', 'released_key_mod' : 'nul', 'pressed' : False},
    15 : {'PIN' : 15, 'pressed_key' : 'PITOT_HEAT', 'released_key' : 'PITOT_HEAT', 'pressed_key_mod' : 'nul', 'released_key_mod' : 'nul', 'pressed' : False},
    16 : {'PIN' : 16, 'pressed_key' : 'STBY_BAT', 'released_key' : 'STBY_BAT', 'pressed_key_mod' : 'nul', 'released_key_mod' : 'nul', 'pressed' : False},
    22 : {'PIN' : 22, 'pressed_key' : 'ALT', 'released_key' : 'ALT', 'pressed_key_mod' : 'nul', 'released_key_mod' : 'nul', 'pressed' : False},
    21 : {'PIN' : 21, 'pressed_key' : 'BAT', 'released_key' : 'BAT', 'pressed_key_mod' : 'nul', 'released_key_mod' : 'nul', 'pressed' : False},
    19 : {'PIN' : 19, 'pressed_key' : 'AV1_ON', 'released_key' : 'AV1_ON', 'pressed_key_mod' : 'nul', 'released_key_mod' : 'nul', 'pressed' : False},
    24 : {'PIN' : 24, 'pressed_key' : 'AV2_ON', 'released_key' : 'AV2_ON', 'pressed_key_mod' : 'nul', 'released_key_mod' : 'nul', 'pressed' : False}
}

# These dicts are used for status updates
pressed_true = {'pressed' : True}
pressed_false = {'pressed' : False}

# Initialises GPIO and adds pressed events for each button
def initMappings():
    for button in mapping_dict.items():
        GPIO.setup(button[1]['PIN'], GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.add_event_detect(button[1]['PIN'], GPIO.FALLING, callback=button_pressed, bouncetime=75)

# Sends a key stroke
def write_report(key_mod,key_code):
    print('Sending keystroke ' + str(key_mod) + ', ' + str(key_code))
    report = (chr(int(key_mod)) + chr(int(mod_dict['nul'])) + chr(int(key_code)) + chr(int(mod_dict['nul'])) *5)
    #-------- modifier------------ mfr reserved null ------------- key press ---------------- 5 pad nulls ========
    with open('/dev/hidg0', 'rb+') as fd:
        fd.write(report.encode())

    time.sleep(release_delay)

    # now release all keys by sending 8 nulls
    report = (chr(int(mod_dict['nul'])) * 8)
    with open('/dev/hidg0', 'rb+') as fd:
        fd.write(report.encode())

    time.sleep(1)

# Performs the "pressed" action for the given button
def button_pressed(channel):
    ispressed = mapping_dict[channel]['pressed']

    if ispressed:
        write_report(mod_dict[mapping_dict[channel]['released_key_mod']],key_dict[mapping_dict[channel]['released_key']])
        mapping_dict[channel].update(pressed_false)
    else:
        write_report(mod_dict[mapping_dict[channel]['pressed_key_mod']],key_dict[mapping_dict[channel]['pressed_key']])
        mapping_dict[channel].update(pressed_true)

# Main program
try:
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BOARD)

    initMappings()

    while True:
        time.sleep(0.01)
        continue
finally:
    GPIO.cleanup() # Clean up
