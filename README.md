# C172 Lights and Power Panel - version 1.0.0

# What is this ?
![Panel pic](https://gitlab.com/vinpav/fshid/-/raw/main/resources/Real_panel.png)

The C172 Lights and Power Panel is a simple and cheap panel featuring toggle and rocker switches. It was designed for flight simulation purposes and inspired by the C172 Skyhawk.
___
# How does it work :

The C172 Lights and Power Panel was built keeping in mind 2 guidelines :

* __Simplicity__ : this is my first realisation, and I wanted to keep it as simple as possible. So, this panel only requires minimal crafting and coding skills.
* __Good looking__  : because even simple objects are more enjoyable to use if they are nice to look at.

The C172 Lights and Power Panel is built on a cheap Raspberry Pi Zero configured in "gadget mode", so it can emulate a keyboard. With the Pi Zero connected to a USB port of the computer, each switch on the panel simply sends its corresponding keystroke to interact with the simulation / game / application / whatever runs on your computer.

* __Benefits__ : no tough setup and configuration required, plus it will work with any simulation / game / application you want.
* __Drawbacks__ : this simple design does not allow feedback from the simulation, which means no fancy landing gear lights or radio frequency display on the panel. If you're looking for a truly interactive home cockpit panel for your sim, you're likely to take a look [elsewhere](https://www.google.com/url?q=https://github.com/scott-vincent/instrument-panel&sa=D&source=editors&ust=1633352473828000&usg=AOvVaw3hIMC2jjgBfncT3yp798C2) (*note that I haven't tested that yet*).


___
# Hardware list

*Yes, I said "cheap" panel and you might think it's not. That being said, you may already have certain parts among those listed. Also note that I didn't look for the lower prices and keep in mind that you can reuse most of these parts for other projects.*

### Required parts
- Panel : 0 € (recycle !)
- Raspberry Pi Zero WH : 14.60 €
- USD to microUSB cable : 2.95 €
- 16 Gb MicroSD card : 9.99 €
- Jumper wires : 9.99 €
- 8 toggle switches : 10.99 €
- 2 rocker switches : 6.29 €
- 10k ohm resistors (pack of 5) : 2.17 €

__Total : 56.98 € (65,78 USD)__

### Optional parts
- Breadboard : 4.99 €
- Carbon fiber vinyl wrap : 11.09 €
- Car door edge guard : 10.39 €
- Raspberry Pi Zero WH case : 2.90 €

__Total with optional parts : 86.35 € (99,69 USD)__

*At last, the pleasure I had (and that I hope you'll have too) to design, code and build this panel was worth the expense.*

***

# Build the panel :

*I'll try to describe each step with as much detail as possible and / or include all useful links and resources, so that (hopefully) you won't need to spend time searching for "how to do this or that" websites to get your panel to work.*

## 1. Setting up the Raspberry Pi Zero

### Hardware :

* Raspberry Pi Zero WH : the panel's brain.
    * *Note : the basic Raspberry Pi Zero, without WiFi chip and soldered GPIO Header, will do the job as much as the "WH". My preference goes to the "WH" because of the comfort given by the WiFi connection and the fact that I'm afraid of soldering irons.*
    * __Warning : this project will only run on the Raspberry Pi Zero model . Any other Pi models won't work as they cannot be configured as USB Gadgets.__
* 4 Gb or more microSD card for the operating system installation and the C172 Lights and Power Panel files.
* A microSD to SD card adapter.
* A computer with a SD card reader.
* MicroUSB to USB cable for powering the Pi and transmit data.

### Software installation :

* __Download__ [Raspberry Pi Imager](https://www.google.com/url?q=https://www.raspberrypi.org/software/&sa=D&source=editors&ust=1633352473831000&usg=AOvVaw04n4qYd8GdRiZJopd8LaW8) and burn *Raspberry Pi OS Lite* on your microSD card. (Alternatively, you can download the [Raspberry Pi OS Lite](https://www.google.com/url?q=https://www.raspberrypi.org/software/operating-systems/&sa=D&source=editors&ust=1633352473831000&usg=AOvVaw0eEigxrJSBurGvPF6lv28F) image and burn it onto your microSD card using [Balena Etcher](https://www.google.com/url?q=https://www.balena.io/etcher/&sa=D&source=editors&ust=1633352473831000&usg=AOvVaw0CQ88bUIFJkEKwMN9N4tiw).)
* __Configure__ a wireless network connection  : [follow these instructions](https://www.google.com/url?q=https://www.raspberrypi.org/documentation/computers/configuration.html%23setting-up-a-headless-raspberry-pi&sa=D&source=editors&ust=1633352473832000&usg=AOvVaw1mOdPEQHFUU8z3zXV4RoeI). Skip this if you're using an ethernet connection.
* __Enable ssh__  : Create an empty file named ssh onto the "boot" partition of the microSD card. This enables remote access. ([more](https://www.google.com/url?q=https://www.raspberrypi.org/documentation/computers/remote-access.html%23ssh&sa=D&source=editors&ust=1633352473832000&usg=AOvVaw2qZLteTpeUf1RrXPpyP4Em))
* Done for the microSD card prep : insert it into your Raspberry Pi Zero and __plug it to your computer__ with the USB cable. Wait a few minutes for the first boot until the green LED stops to blink.
* __Connect__ to your Raspberry Pi Zero using ssh : [follow these instructions](https://www.google.com/url?q=https://www.raspberrypi.org/documentation/computers/remote-access.html%23remote-access&sa=D&source=editors&ust=1633352473833000&usg=AOvVaw2HBOFoz1kfYFiC2tAnhVfA).
* On your Raspberry Pi Zero, __download and install__ the C172 Lights and Power Panel archive :

```sh
cd /home/pi
wget https://gitlab.com/vinpav/fshid/-/raw/main/release/fshid_lastest.tar.gz
tar -xzvf fshid_latest.tar.gz
```

* Turn your Raspberry Pi Zero into a __USB Keyboard__ : [follow steps 1 to 3 of this guide](https://www.google.com/url?q=https://randomnerdtutorials.com/raspberry-pi-zero-usb-keyboard-hid/&sa=D&source=editors&ust=1633352473834000&usg=AOvVaw2DzwVumTMWJ-djQOkYvAcw).
* Configure the Raspberry Pi Zero to execute the C172 Lights and Power Panel launcher at startup :
```sh
sudo nano /etc/rc.local
```
Add this before the line containing exit 0  :
```sh
/home/pi/fshid/start_fshid.sh
```
* __Reboot__ the Raspberry Pi Zero
```sh
sudo reboot
```
____
## 2. Testing

*Once you're done with the installation, you might want to test if the Raspberry Pi Zero is capable of sending keystrokes to your computer. This requires additional pieces of hardware and a bit of wiring.*

### Hardware :

* [Toggle switch](https://www.google.com/url?q=https://www.amazon.fr/gp/product/B07M85FZQL/ref%3Dppx_yo_dt_b_asin_title_o09_s00?ie%3DUTF8%26psc%3D1&sa=D&source=editors&ust=1633352473836000&usg=AOvVaw2YmOtcliAef2fgOKSqJyWR) : at this step you need only one, but you'll need 8 of them for the whole panel.
* [10k ohm Resistor](https://www.google.com/url?q=https://www.amazon.com/s?k%3D10k%2Bohm%2Bresistor%26crid%3D2RVSV6WXNSRBZ%26sprefix%3D10k%2Bo%252Caps%252C276%26ref%3Dnb_sb_ss_ts-doa-p_1_5&sa=D&source=editors&ust=1633352473836000&usg=AOvVaw374DBBTDDoa_KPRrJYbQ9d) : only one needed for the whole panel (you may not want to buy a 500+ pack...)
* [Jumper wires](https://www.google.com/url?q=https://www.amazon.com/Haitronic-Multicolored-Breadboard-Arduino-raspberry/dp/B01LZF1ZSZ/ref%3Das_li_ss_tl?s%3Dindustrial%26ie%3DUTF8%26qid%3D1515716745%26sr%3D1-3%26keywords%3Djumper%2Bwires%26linkCode%3Dsl1%26tag%3Drasphq_electronics-online-20%26linkId%3Dff4a1e3598b13434e8f1313eb274fbf4&sa=D&source=editors&ust=1633352473837000&usg=AOvVaw2CwI_ueqYEkp7p75IV50gi)
* Optional : [Breadboard](https://www.google.com/url?q=https://www.amazon.com/gp/product/B013DLU6I8/ref%3Das_li_qf_asin_il_tl?ie%3DUTF8%26tag%3Drapihq-20%26creative%3D9325%26linkCode%3Das2%26creativeASIN%3DB013DLU6I8%26linkId%3D3f1db938c8af386fe1e52e92db5921b2&sa=D&source=editors&ust=1633352473836000&usg=AOvVaw2UdNQ07eL7qp2Yyk20le93) : makes the wiring easier, can replace the electric wires.

### Wiring and testing :

* Wire as follows :

![Wiring diagram](https://gitlab.com/vinpav/fshid/-/raw/main/resources/wiring_test.jpg)
* Plug your Raspberry Pi Zero to your computer and wait for it to boot. Windows users may see a notification telling that a USB device is not recognised : don't worry about it.
* Open your favorite text editor and toggle the switch.
* The number 3 should appear, meaning you're ready for the next step !

> __Troubleshooting : if the Panel is not sending the keystrokes, please ensure that :__
> 
> * You have properly plugged the USB cable in the Data microUSB port of the Raspberry Pi Zero. (Pro tip : this is not the one located near a corner of the card).
> * Your USB cable is capable of transporting data. Some USB cables, like those provided with power banks, aren't data capable.


***

## 3. Design

*The C172 Lights and Power Panel is basically a panel (with holes). Thus, the design isn't very complicated.*

Here's everything you need to know :

![Tech draw](https://gitlab.com/vinpav/fshid/-/raw/main/resources/tech_draw.png)

[PDF version](https://gitlab.com/vinpav/fshid/-/blob/main/resources/C172_panel_metal_small-Page.pdf)

For those who feel like making the panel fit better on their desk, [here's the FreeCAD project file](https://gitlab.com/vinpav/fshid/-/blob/main/resources/C172_panel_metal_small.FCStd).

Please notice that you'll have to figure out how to make your panel stand up / how to fix it, depending on your desk configuration.
___
## 4. Making

*Let's delve into the depths of craftsmanship...*

### Hardware :

* [8 toggle switches](https://www.google.com/url?q=https://www.amazon.fr/gp/product/B07M85FZQL/ref%3Dppx_yo_dt_b_asin_title_o09_s00?ie%3DUTF8%26psc%3D1&sa=D&source=editors&ust=1633352473840000&usg=AOvVaw0YO7-n4SKmVpsR13DbFdMB)
* [2 double rocker switches](https://www.google.com/url?q=https://www.amazon.fr/gp/product/B0743BMMGG/ref%3Dppx_yo_dt_b_asin_title_o02_s00?ie%3DUTF8%26psc%3D1&sa=D&source=editors&ust=1633352473840000&usg=AOvVaw25RaKaFxgM_cnwB3aQ1bPj)
* *Optional : [carbon fiber vinyl wrap](https://www.google.com/url?q=https://www.amazon.fr/gp/product/B088TBFMKM/ref%3Dppx_yo_dt_b_asin_title_o04_s00?ie%3DUTF8%26psc%3D1&sa=D&source=editors&ust=1633352473841000&usg=AOvVaw1CPVqMRjr2_WTbCbJKeakM)*
* *Optional : [car door edge guard](https://www.google.com/url?q=https://www.amazon.fr/gp/product/B01MY98SIC/ref%3Dppx_yo_dt_b_asin_title_o03_s00?ie%3DUTF8%26psc%3D1&sa=D&source=editors&ust=1633352473841000&usg=AOvVaw2ubsjJsm4HD_wNXRI8JuGi)*
* Panel (minimum dimensions 350 x 150 mm)

*About the panel : you can use the material you want (wood, metal, plastic). Keep in mind that if your panel is lightweight you'll have to fix it on your desk. Mine was made out of an old metallic desk rack that is heavy enough to stand still while I operate the switches.*

### Building Steps :

1. Cut the panel at the required dimensions and drill the holes OR Print the panel on a 3D printer (warning, the provided FreeCAD project wasn't made for 3D printing, it may need adjustments).
2. *Optional : for a better look, apply the carbon fiber vinyl wrap or paint the panel.*
3. Paint or stick the labels.
4. Position the toggle switches and screw the nuts to fix them.
5. Position the rocker switches and carefully press them until they're blocked.
6. *Optional : apply the car door edge guard on the visible edges of the panel*

***

## 5. Wiring

*Let's put everything together and finalise the C172 Lights and Power Panel.*

Wiring is no more than repeating a button wiring 12 times :

![Wiring diagram](https://gitlab.com/vinpav/fshid/-/raw/main/resources/C172_Lights_Power_Panel_wiring.jpg)

In real life, here's what it looks like :

![Wiring pic](https://gitlab.com/vinpav/fshid/-/raw/main/resources/Real_panel_rear.png)

Once this last step is done, there you go ! Plug your brand new C172 Lights and Power Panel to your computer, start your favorite flight simulator, bind your panel's keys and FLY !

___
# Normal use

## Start-up
> __Important notice__ : before starting the panel, be sure that all switches are in the OFF position, so that the switches statuses in the software correspond to the physical switches position.

To start the C172 Lights and Power panel, simply plug it on a USB port of your computer. If you leave the panel plugged to your computer, it will start when you turn your computer on like any USB device.

## Shutdown
### Quick and dirty method
If you don't want to bother too much, just turn your computer off / unplug the USB cable.

However, be aware that this electric shutdown method presents a risk of micro SD card corruption, as the Raspberry Pi Zero may be reading or writing data when the power runs out. So, if you regularly shutdown your panel with this method, think of creating a backup image of the micro SD card.

### Proper method
- Connect to the Raspberry Pi Zero with ssh.
- Enter this command : `sudo shutdown now`
- Wait a few seconds for the Raspberry Pi Zero green LED to turn off.

## Application keybinds configuration
When using the C172 Lights and Power Panel for the first time with an application, don't forget to configure the panel's keybinds in that application.

> __Microsoft Flight Simulator 2020 users__ : the C172 Lights and Power Panel doesn't show in the device list of the configuration controls page. Configure it on the keyboard tab.

___
# Key bindings

## Default keys
Here are the default keystrokes of the C172 Lights and Power Panel :
| Entry Name | Corredponding key |
| ------ | ------ |
| BEACON | Keypad 1 |
| LANDING_LIGHT | Keypad 2 |
| TAXI_LIGHT | Keypad 3 |
| NAV_LIGHT | Keypad 4 |
| STROBE_LIGHT | Keypad 5 |
| FUEL_PUMP | Keypad 6 |
| PITOT_HEAT | Keypad 7 |
| STBY_BAT | Keypad 8 |
| ALT | Keypad 9 |
| BAT | Keypad 0 |
| AV1_ON | Keyboard [ |
| AV1_OFF | Keyboard ] |
| AV2_ON | Keypad . |
| AV2_OFF | Keypad + |

*Note : if you're ok to use the default keys, you can safely skip the rest of this section.*

## Understanding the configuration
Connect to the C172 Lights and Power Panel and open the following file :
```sh
nano /home/pi/fshid/fshid.py
```
This file contains 3 data structures (AKA dicts) which are responsible of keys definition and bindings to the switches :
- __mod_dict__ contains the key codes for the supported modifiers. You will never have to modify this dict.
- __key_dict__ contains a list of keys codes. Each key is defined by a name that you can define freely, and a key code from the keyboard table of the [USB HID usage tables](https://www.usb.org/sites/default/files/documents/hut1_12v2.pdf) (see page 53). You can modify this dict as you want to fit your needs (see Basic configuration below).
- __mapping_dict__ contains all the needed data to bind keys and switchs, allowing you to define witch keystroke is sent whether the switch is up or down. (see Advanced configuration below).

## Basic configuration : Updating the key codes
The default keys defined in the __key_dict__ may not work with your own setting, so you'll need to change them.

Again, refer to the keyboard table of the [USB HID usage tables](https://www.usb.org/sites/default/files/documents/hut1_12v2.pdf) (page 53) and replace the existing key code with a value of the firt column of the table.
__Example__ : 'BEACON':89 means that the key code 89, which refers to the "keypad 1" key of a keyboard, will be sent for the BEACON switch. If you want the switch to send the 'b' key instead, simple replace by the corresponding key code as follow : 'BEACON':5

*Note : You can add as many keys you want in the key_dict*

## Advanced configuration
### mapping_dict format
The __mapping_dict__ associates a key to a switch position. Each line has the following format :
```
PIN : {'PIN' : 'button_pin', 'pressed_key' : 'key_dict_entry', 'released_key' : 'key_dict_entry', 'pressed_key_mod' : 'mod_dict_entry', 'released_key_mod' : 'mod_dict_entry', 'pressed' : 'button status True or False'}
```
Where :
- `PIN / button_pin` is the GPIO pin number connected to the switch. You shouldn't have to update this value unless your wiring differs from the default wiring (see section 5).
- `pressed_key` is a key name from the key_dict, representing the key that is sent when the switch (button) is on (pressed).
- `released_key` is a key name from the key_dict, representing the key that is sent when the switch (button) is off (released).
- `pressed_key_mod` is a modifier name from the mod_dict that is used with the pressed_key.
- `released_key_mod` is a modifier name from the mod_dict that is used with the released_key.
- `pressed` represents current the button position. You must not update this value.

    __Example__ : let's have a look at the "Avionics Bus1" switch default configuration.
    ```
    19 : {'PIN' : 19, 'pressed_key' : 'AV1_ON', 'released_key' : 'AV1_OFF', 'pressed_key_mod' : 'nul',  'released_key_mod' : 'nul', 'pressed' : False},
    ```
    This mapping_dict entry tells that the switch connected to pin 19 sends the 'AV1_ON' key with a 'nul' modifier when it is switched on, and the 'AV1_OFF' key with a 'nul' modifier when it is switched off.

### Using modifiers
Use modifiers if you want the switch to send a combination of keys to the sim instead of a simple key.
Existing modifiers are the following :
| Code | Associated modifier key |
| ------ | ------ |
| nul | No modifier |
| shf | Shift |
| ctl | Ctrl |
| alt | Alt |
| ctlshf | Ctrl + Shift |
| altshf | Alt + Shift |
| altctl | Alt + Ctrl |

According to this table, the syntax for the first switch (BEACON, by default) for sending the `Shift + b` keystroke when switched on, and `Alt + Shift + b` when switched off would be :
```
# key_dict entry :
'BEACON':5,
# mapping_dict entry
7 : {'PIN' : 7, 'pressed_key' : 'BEACON', 'released_key' : 'BEACON', 'pressed_key_mod' : 'shf', 'released_key_mod' : 'altshf', 'pressed' : False},
```
### Binding more switches and buttons
You can add as many lines in __mapping_dict__ as there are switches connected to GPIO Pins of the Raspberry Pi Zero.
New mappings will be automatically initialised after a reboot of your C172 Lights and Power Panel.
___
# Want more ?

- You may notice that the C172 Lights and Power Panel doesn't use all GPIO Pins, many of them are left unused : as many possibilities of wiring some other switches and buttons.
- This panel is just a proposition based on the C172 Skyhawk : let your creativity speak and shape this solution to your favorite aircraft !
___
# Improvements

* Add a "shutdown" button on the panel, so that you can properly turn off the Raspberry Pi Zero before the PC shuts down, preventing sd card corruption.
* Add different fixation systems (desk / wall / ...) on the FreeCAD project.
___
# Many thanks to

- My friend Moostyk for his metal working skills.
- [This guy](https://github.com/kpmn819/FS-Release-Candidate) for the "write_report" python method !
- [The Flying Fabio](https://theflyingfabio.com/) for giving me the motivation to build this panel through his awesome quality streams.

